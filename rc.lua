-- ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
-- ━┏━━━┓━━━━━━━━━━━━┏┓━━━━━━━━┏━━━┓━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
-- ━┃┏━┓┃━━━━━━━━━━━━┃┃━━━━━━━━┃┏━┓┃━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
-- ━┃┃━┃┃┏━━┓┏━━┓┏┓┏┓┗┛┏━━┓━━━━┃┃━┃┃┏┓┏┓┏┓┏━━┓┏━━┓┏━━┓┏┓┏┓┏━━┓━━━━┏━┓┏━━┓━
-- ━┃┗━┛┃┃┏┓┃┃┏┓┃┃┃┃┃━━┃ ━┫━━━━┃┗━┛┃┃┗┛┗┛┃┃┏┓┃┃━━┫┃┏┓┃┃┗┛┃┃┏┓┃━━━━┃┏┛┃┏━┛━
-- ━┃┏━┓┃┃┗┛┃┃┗┛┃┃┗┛┃━━┣━ ┃━━━━┃┏━┓┃┗┓┏┓┏┛┃┗━┫┣━━┃┃┗┛┃┃┃┃┃┃┗━┫━━━━┃┃━┃┗━┓━
-- ━┗┛━┗┛┃┏━┛┃┏━┛┗━━┛━━┗━━┛━━━━┗┛━┗┛━┗┛┗┛━┗━━┛┗━━┛┗━━┛┗┻┻┛┗━━┛━━━━┗┛━┗━━┛━
-- ━━━━━━┃┃━━┃┃━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
-- ━━━━━━┗┛━━┗┛━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
-- ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
-- ━━━━━━━━━━━━━━━━━━━━━━━━━━ Horizon @ texteditor.com/multiline-text-art━
-- ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
--
-- Find LuaRocks packages if installed
pcall(require, "luarocks.loader")

-- Awesome libraries
local awful = require("awful")
local beautiful = require("beautiful")
local naughty = require("naughty")
require("awful.autofocus")
require("awful.hotkeys_popup.keys")

-- Set theme
beautiful.init("~/.config/awesome/theme/theme.lua")

-- Set notification defaults
naughty.config.defaults.position = "bottom_right"

-- My imports
require("modules.rc_error_handling")
require("modules.rc_ewmh")
local keys = require("modules.rc_keys")
require("modules.rc_rules")
require("modules.rc_signals")
require("modules.rc_wibar")

-- Set keys
root.keys(keys.globalkeys)

-- -- Setup env vars
-- awful.spawn.with_shell("/home/appu/.config/fish/conf.d/10-vars.fish")

-- XDG Autostart
awful.spawn.with_shell("pidof nm-applet || dex --environment Awesome --autostart -w &")

-- awful.spawn.with_shell(
--     'if (xrdb -query | grep -q "^awesome\\.started:\\s*true$"); then exit; fi;' ..
--     'xrdb -merge <<< "awesome.started:true";' ..
--     -- list each of your autostart commands, followed by ; inside single quotes, followed by ..
--     'dex --environment Awesome --autostart'
--     )

-- Start Firefox on boot on first tag
awful.spawn.with_shell("pidof firefox || firefox &", false)

-- Autolocking
awful.spawn.with_shell("xautolock -time 30 -locker /home/appu/.local/bin/lock &")

-- Finish notification
naughty.notify({
	title = "rc.lua loaded!",
	text = "The whole config file has been ran",
	timeout = 1,
	position = "top_middle",
})
