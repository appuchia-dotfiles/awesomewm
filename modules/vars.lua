-- Variables
local vars = {}

-- Default modkey.
vars.modkey = "Mod4"

-- Key-related
vars.terminal = "kitty"
vars.editor = os.getenv("EDITOR") or "nvim"
vars.cmd_editor = vars.terminal .. " -e " .. vars.editor
vars.cmd_browser = "firefox"
vars.cmd_spotify = "spotify"
vars.cmd_file_explorer = "thunar"
vars.cmd_discord = "discord"
vars.cmd_rofi_prefix = "rofi -config ~/.config/rofi.appu/config.rasi "
vars.cmd_rofi_code = "ls ~/Code | rofi -dmenu -p 'repo' -i -window-title Code | xargs printf '~/Code/%s' | xargs -r kitty -d"
-- find ~/Code -iname '*.code-workspace' | rofi -dmenu -p 'repo' -i -window-title Code | xargs printf '~/Code/%s' | xargs -r code

vars.spotify_control =
    "dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player."

-- Tags
-- vars.tags = {{"|", "||", "|||", "|\\/", "\\/", "\\/|", "\\/||"}, {"I", "II", "III", "IV", "V", "VI", "VII"}}
vars.tags = {" I  ", "II ", "III ", "IV ", " V  ", "VI ", "VII ", "VIII", "IX "}

return vars
