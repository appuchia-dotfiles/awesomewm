local awful = require("awful")
local beautiful = require("beautiful")
local keys = require("modules.rc_keys")
local vars = require("modules.vars")

-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
	{ --
		rule = {}, -- All clients will match this rule.
		properties = {
			border_width = beautiful.border_width,
			border_color = beautiful.border_normal,
			focus = awful.client.focus.filter,
			raise = true,
			keys = keys.clientkeys,
			buttons = keys.clientbuttons,
			screen = awful.screen.preferred,
			titlebars_enabled = false,
			placement = awful.placement.no_offscreen,
			-- placement = awful.placement.centered + awful.placement.no_overlap + awful.placement.no_offscreen
		},
	},
	{ -- Floating clients.
		rule_any = {
			instance = { "DTA", "copyq", "pinentry" },
			class = {
				-- "Bitwarden",
				-- "Gpick",
				-- "Kruler",
				-- "MessageWin",
				-- "Sxiv",
				-- "stacer",
				"Arandr",
				"Authenticator", -- Yubico Authenticator
				"AutoFirma",
				"Arduino IDE",
				"Blueman-manager",
				"Bottles",
				"bottles",
				"Caffeine",
				"DB Browser for SQLite",
				"Fr.handbrake.ghb",
				"GParted",
				"HTTPie",
				"Imager",
				"KeePassXC",
				"Localsend_app",
				"Lxappearance",
				"Minecraft Launcher",
				"Nm-connection-editor",
				"Nvidia-settings",
				"Pavucontrol",
				"PeaZip",
				"Piper",
				"Rustdesk",
				"Signal",
				"Solaar",
				"TeamViewer",
				"Timeshift-gtk",
				"Tor Browser",
				"Upscayl",
				"Virt-manager",
				"VirtualBox Manager",
				"VirtualBox",
				"ark",
				"baobab",
				"com.github.tchx84.Flatseal",
				"converseen",
				"czkawka-gui",
				"easyeffects",
				"gnome-calendar",
				"kdeconnect.app",
				"kdeconnect.sms",
				"krunner",
				"merkuro.calendar",
				"merkuro.contact",
				"monero-core",
				"openrgb",
				"org.remmina.Remmina",
				"processing-app-Base", -- Arduino IDE
				"qBittorrent",
				"qt5ct",
				"qt6ct",
				"zenity",
			},
			name = { -- Note that the name property shown in xprop might be set slightly after creation of the client and the name shown there might not match defined rules here.
				"Event Tester",
				"Friends List", -- Steam
				"Blender Preferences",
			},
			role = {
				"AlarmWindow", -- Thunderbird's calendar.
				"ConfigManager", -- Thunderbird's about:config.
				"pop-up", -- e.g. Google Chrome's (detached) Developer Tools.
			},
		},

		properties = {
			floating = true,
			placement = awful.placement.centered,
		},
	},
	{ -- Add titlebars to normal clients and dialogs
		rule_any = { type = { "dialog" } }, -- It used to include "normal" too
		properties = { titlebars_enabled = true },
	},
	-- Special rules
	{ rule = { class = "kcalc" }, properties = { floating = true, ontop = true } },
	{ rule = { class = "Spotify" }, properties = { tag = vars.tags[9] } },
	{ rule = { class = "discord" }, properties = { tag = vars.tags[8] } },
	{ rule = { class = "thunderbird" }, properties = { tag = vars.tags[9] } },
	{ rule = { class = "Arduino IDE" }, properties = { titlebars_enabled = true } },

	{
		rule = { class = "Vmware-view" },
		properties = {
			floating = true,
			placement = awful.placement.centered,
			width = 1920,
			height = 1116,
		},
	},
}
