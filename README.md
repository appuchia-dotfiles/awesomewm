# AwesomeWM dotfiles

[![GPLv3 license](https://img.shields.io/gitlab/license/appuchia-dotfiles/awesomewm?style=flat-square)](https://gitlab.com/appuchia-dotfiles/awesomewm/-/blob/master/LICENSE)
[![Author](https://img.shields.io/badge/Project%20by-Appu-9cf?style=flat-square)](https://gitlab.com/appuchia)

## What this is

This repo holds all the files contained in my AwesomeWM installation. Feel free to use any bit of them.

## How it looks

> ![Screenshot](Screenshot.png)

## Not yet intended for installation

## License

This project is licensed under the [GPLv3 license](https://gitlab.com/appuchia-dotfiles/awesomewm/-/blob/master/LICENSE).

Coded with 🖤 by Appu
